package jacktgq.moveTheBox;

import jacktgq.AlgoVisHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class AlgoFrame extends JFrame{
    private int canvasWidth;
    private int canvasHeight;

    private GameData gameData;
    public AlgoCanvas canvas;

    public AlgoFrame(String title, int width, int height, GameData gameData){
        super(title);
        this.canvasWidth = width;
        this.canvasHeight = height;
        this.gameData = gameData;

        canvas = new AlgoCanvas();
        setContentPane(canvas);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //setSize(width + 16, height + 39);
        setSize(width + 6, height + 29);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void render(GameData gameData) {
        this.gameData = gameData;
        canvas.repaint();
    }

    public class AlgoCanvas extends JPanel {
        private HashMap<Character, BufferedImage> colorMap;
        //private ArrayList<Color> colorList;

        public AlgoCanvas() {
            // 双缓存
            super(true);

            colorMap = new HashMap<>();
            /*colorList = new ArrayList<>(Arrays.asList(AlgoVisHelper.Red, AlgoVisHelper.Pink, AlgoVisHelper.Purple, AlgoVisHelper.DeepPurple, AlgoVisHelper.Indigo, AlgoVisHelper.Blue, AlgoVisHelper.LightBlue, AlgoVisHelper.Cyan, AlgoVisHelper.Teal, AlgoVisHelper.Green, AlgoVisHelper.LightGreen, AlgoVisHelper.Lime, AlgoVisHelper.Yellow, AlgoVisHelper.Amber, AlgoVisHelper.Orange, AlgoVisHelper.DeepOrange));*/
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);

            int m = gameData.getM();
            int n = gameData.getN();

            int block_width = canvasWidth / n - 1;
            int block_height = canvasHeight / m - 1;

            Board showBoard = gameData.getShowBoard();
            for (int i = 0; i < showBoard.getM(); i++) {
                for (int j = 0; j < showBoard.getN(); j++) {
                    char c = showBoard.getDataAt(i, j);
                    if (c != Board.EMPTY) {
                        if (!colorMap.containsKey(c)) {
                            int size = colorMap.size();
                            colorMap.put(c, GameData.BOX_IMAGES[size]);
                        }
                        BufferedImage image = colorMap.get(c);
                        AlgoVisHelper.putImage(g2d, j * block_width - 2, i * block_height - 2, image);
                        /*AlgoVisHelper.setColor(g2d, AlgoVisHelper.White);
                        AlgoVisHelper.drawText(g2d, String.format("(%d, %d)", i, j), j * block_width + block_width / 2, i * block_height + block_height / 2);*/
                    }
                }
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


