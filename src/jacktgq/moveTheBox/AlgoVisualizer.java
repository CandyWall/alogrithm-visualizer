package jacktgq.moveTheBox;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AlgoVisualizer {
    private AlgoFrame frame;

    private static final int BLOCK_WIDTH = 98;
    private static final int BLOCK_HEIGHT = 98;

    private GameData gameData;
    private AlgoMouseListener mouseListener;

    private int M, N;

    public AlgoVisualizer(String filename){
        gameData = new GameData(filename);

        this.M = gameData.getM();
        this.N = gameData.getN();
        
        int sceneWidth = BLOCK_WIDTH * N;
        int sceneHeight = BLOCK_HEIGHT * M;
        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("MoveTheBox", sceneWidth, sceneHeight, gameData);
            frame.addKeyListener(new AlgoKeyListener());
            mouseListener = new AlgoMouseListener();
            frame.canvas.addMouseListener(mouseListener);
            new Thread(() -> {
                run();
            }).start();
        });
    }

    private void run() {
        setData(true, -1, -1);

        if (gameData.solve()) {
            System.out.println("游戏有解！");
        } else {
            System.out.println("游戏无解！");
        }
    }

    private class AlgoKeyListener extends KeyAdapter {
        @Override
        // 键盘按下事件
        public void keyReleased(KeyEvent e) {

        }
    }

    private void setData(boolean isLeftClicked, int x, int y) {

    }

    private class AlgoMouseListener extends MouseAdapter {
        @Override
        public void mouseReleased(MouseEvent event){
        }
    }

    public static void main(String[] args) {
        String filename = "data/moveTheBox.txt";
        AlgoVisualizer visualizer = new AlgoVisualizer(filename);
    }
}
