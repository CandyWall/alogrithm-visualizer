package jacktgq.fractalImage.vicsek;

/**
 * @Author CandyWall
 * @Date 2021/3/28--23:04
 * @Description
 */
public class FractalData {
    private int depth;

    public FractalData(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}
