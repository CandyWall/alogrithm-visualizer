package jacktgq.fractalImage.tree;

import jacktgq.AlgoVisHelper;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AlgoVisualizer {
    private static final int DELAY = 40;
    private AlgoFrame frame;

    private FractalData fractalData;

    public AlgoVisualizer(int sceneWidth, int sceneHeight, int maxDepth, double splitAngle){
        fractalData = new FractalData(0, splitAngle);

        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("绘制一棵分形二叉树", sceneWidth, sceneHeight, fractalData);
            frame.addKeyListener(new AlgoKeyListener());
        });
    }

    private class AlgoKeyListener extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            char keyChar = e.getKeyChar();
            if (keyChar >= '0' && keyChar <= '9') {
                setData(keyChar - '0');
            }
        }
    }

    private void setData(int depth) {
        if (depth >= 0) {
            fractalData.setDepth(depth);
        }
        frame.render(fractalData);
        AlgoVisHelper.pause(DELAY);
    }

    public static void main(String[] args) {
        int maxDepth = 6;
        double splitAngle = 60.0;
        int sceneWidth = 800;
        int sceneHeight = 800;
        AlgoVisualizer visualizer = new AlgoVisualizer(sceneWidth, sceneHeight, maxDepth, splitAngle);
    }
}
