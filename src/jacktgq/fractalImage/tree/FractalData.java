package jacktgq.fractalImage.tree;

/**
 * @Author CandyWall
 * @Date 2021/3/28--23:04
 * @Description
 */
public class FractalData {
    private int depth;
    private double splitAngle;

    public FractalData(int depth, double splitAngle) {
        this.depth = depth;
        this.splitAngle = splitAngle;
    }

    public int getDepth() {
        return depth;
    }

    public double getSplitAngle() {
        return splitAngle;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
}
