package jacktgq.fractalImage.kochSnowflake;

import jacktgq.AlgoVisHelper;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AlgoVisualizer {
    private static final int DELAY = 40;
    private AlgoFrame frame;

    private FractalData fractalData;

    public AlgoVisualizer(int maxDepth){
        fractalData = new FractalData(0);

        int sceneWidth = (int) Math.pow(3, maxDepth);
        int sceneHeight = (int) Math.pow(3, maxDepth);

        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("DrawKochSnowFlake", sceneWidth, sceneHeight, fractalData);
            frame.addKeyListener(new AlgoKeyListener());
        });
    }

    private class AlgoKeyListener extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
            char keyChar = e.getKeyChar();
            if (keyChar >= '0' && keyChar <= '9') {
                setData(keyChar - '0');
            }
        }
    }

    private void setData(int depth) {
        if (depth >= 0) {
            fractalData.setDepth(depth);
        }
        frame.render(fractalData);
        AlgoVisHelper.pause(DELAY);
    }

    public static void main(String[] args) {
        AlgoVisualizer visualizer = new AlgoVisualizer(6);
    }
}
