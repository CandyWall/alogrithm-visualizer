package jacktgq.fractalImage.kochSnowflake;

import jacktgq.AlgoVisHelper;

import javax.swing.*;
import java.awt.*;

public class AlgoFrame extends JFrame{
    private int canvasWidth;
    private int canvasHeight;

    private FractalData fractalData;
    public AlgoCanvas canvas;

    public AlgoFrame(String title, int width, int height, FractalData fractalData){
        super(title);
        this.canvasWidth = width;
        this.canvasHeight = height;
        this.fractalData = fractalData;

        canvas = new AlgoCanvas();
        setContentPane(canvas);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //setSize(width + 16, height + 39);
        setSize(width + 6, height + 29);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void render(FractalData fractalData) {
        this.fractalData = fractalData;
        canvas.repaint();
    }

    public class AlgoCanvas extends JPanel {
        public AlgoCanvas() {
            // 双缓存
            super(true);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);
            // 具体绘制
            drawFractal(g2d, 0, canvasHeight - 3, canvasWidth, 0, 0);
        }

        private void drawFractal(Graphics2D g2d, double x1, double y1, int side, double angle,  int depth) {
            if (side <= 0) {
                return;
            }

            if (depth == fractalData.getDepth()) {
                double x2 = x1 + side * Math.cos(angle * Math.PI / 180.0);
                double y2 = y1 - side * Math.sin(angle * Math.PI / 180.0);
                AlgoVisHelper.setColor(g2d, AlgoVisHelper.Indigo);
                AlgoVisHelper.drawLine(g2d, x1, y1, x2, y2);
            }

            int side_3 = side / 3;
            double x2 = x1 + side_3 * Math.cos(angle * Math.PI / 180.0);
            double y2 = y1 - side_3 * Math.sin(angle * Math.PI / 180.0);
            drawFractal(g2d, x1, y1, side_3, angle, depth + 1);

            double x3 = x2 + side_3 * Math.cos((angle + 60.0) * Math.PI / 180.0);
            double y3 = y2 - side_3 * Math.sin((angle + 60.0) * Math.PI / 180.0);
            drawFractal(g2d, x2, y2, side_3, angle + 60.0, depth + 1);

            double x4 = x3 + side_3 * Math.cos((angle - 60.0) * Math.PI / 180.0);
            double y4 = y3 - side_3 * Math.sin((angle - 60.0) * Math.PI / 180.0);
            drawFractal(g2d, x3, y3, side_3, angle - 60.0, depth + 1);

            drawFractal(g2d, x4, y4, side_3, angle, depth + 1);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


