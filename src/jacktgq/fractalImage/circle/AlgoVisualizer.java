package jacktgq.fractalImage.circle;

import java.awt.*;

public class AlgoVisualizer {
    private AlgoFrame frame;

    private CircleData circleData;

    public AlgoVisualizer(int sceneWidth, int sceneHeight){
        int R = Math.min(sceneWidth, sceneHeight) / 2 - 2;
        circleData = new CircleData(sceneWidth / 2, sceneHeight / 2, R, R / 2, 2);

        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("DrawCircle", sceneWidth, sceneHeight, circleData);
        });
    }

    public static void main(String[] args) {
        AlgoVisualizer visualizer = new AlgoVisualizer(800, 800);
    }
}
