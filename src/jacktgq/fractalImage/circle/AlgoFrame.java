package jacktgq.fractalImage.circle;

import jacktgq.AlgoVisHelper;
import jacktgq.moveTheBox.Board;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class AlgoFrame extends JFrame{
    private int canvasWidth;
    private int canvasHeight;

    private CircleData circleData;
    public AlgoCanvas canvas;

    public AlgoFrame(String title, int width, int height, CircleData circleData){
        super(title);
        this.canvasWidth = width;
        this.canvasHeight = height;
        this.circleData = circleData;

        canvas = new AlgoCanvas();
        setContentPane(canvas);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //setSize(width + 16, height + 39);
        setSize(width + 6, height + 29);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void render(CircleData circleData) {
        this.circleData = circleData;
        canvas.repaint();
    }

    public class AlgoCanvas extends JPanel {
        public AlgoCanvas() {
            // 双缓存
            super(true);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);

            // 具体绘制
            drawCircle(g2d, circleData.getStartX(), circleData.getStartY(), circleData.getStartR(), 0);
        }

        private void drawCircle(Graphics2D g2d, int x, int y, int r, int depth) {
            if (depth == circleData.getDepth() || r < 1) {
                return;
            }
            /*if (depth % 2 == 0) {
                AlgoVisHelper.setColor(g2d, AlgoVisHelper.Red);
            } else {
                AlgoVisHelper.setColor(g2d, AlgoVisHelper.LightBlue);
            }*/
            int rr = (int) (Math.random() * 256);
            int gg = (int) (Math.random() * 256);
            int bb = (int) (Math.random() * 256);
            Color randomColor = new Color(rr, gg, bb);
            AlgoVisHelper.setColor(g2d, randomColor);

            AlgoVisHelper.fillCircle(g2d, x, y, r);
            drawCircle(g2d, x, y, r - circleData.getStep(), depth + 1);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


