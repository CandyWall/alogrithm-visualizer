package jacktgq.probability.getPi;
import jacktgq.AlgoVisHelper;

import java.awt.EventQueue;
import java.awt.Point;
import java.util.ArrayList;

public class AlgoVisualizer {

    // TODO: 创建自己的数据
    private ArrayList<Point> points;        // 数据
    private AlgoFrame frame;    // 视图
    private long insideCircleCount = 0;

    public AlgoVisualizer(int sceneWidth, int sceneHeight){

    	// 存一万个坐标
    	points = new ArrayList<Point>();
    	
    	
        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("使用蒙特卡洛算法模拟计算PI", sceneWidth, sceneHeight, points);
            new Thread(() -> {
                run();
            }).start();
        });
    }

    // 动画逻辑
    private void run(){
		// 随机产生一万个像素坐标
    	for(int i = 0; i < 100000; i++) {
    		Point point = new Point();
    		point.x = (int) (Math.random() * frame.getCanvasWidth());
    		point.y = (int) (Math.random() * frame.getCanvasHeight());
    		points.add(point);
    		//统计在圈内的点数
    		if(frame.circle.contains(point))
    			insideCircleCount++;
    		frame.render(points);
    		if(i % 100 == 0) {
    			AlgoVisHelper.pause(40);
    			System.out.println("π的近似值为：" + 4.0 * insideCircleCount / points.size());
    		}
    	}
    }
    
    public static void main(String[] args) {

        int sceneWidth = 800;
        int sceneHeight = 800;

        // TODO: 根据需要设置其他参数，初始化visualizer
        AlgoVisualizer visualizer = new AlgoVisualizer(sceneWidth, sceneHeight);
    }
}
