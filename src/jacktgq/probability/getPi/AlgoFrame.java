package jacktgq.probability.getPi;
import jacktgq.AlgoVisHelper;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.*;
/**
 * 
 * @Title: AlgoFrame.java 
 * @Package top.jacktgq.probability.getPi 
 * @Description: 根据蒙特卡洛算法模拟计算PI，并进行可视化
 * @author CandyWall   
 * @date 2020年10月14日 下午11:38:51 
 * @version V1.0
 */
public class AlgoFrame extends JFrame{

    private int canvasWidth;
    private int canvasHeight;
    public Ellipse2D.Double circle = null;

    public AlgoFrame(String title, int canvasWidth, int canvasHeight, ArrayList<Point> points){

        super(title);
        circle = new Ellipse2D.Double(0, 0, canvasWidth, canvasHeight);
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.points = points;

        AlgoCanvas canvas = new AlgoCanvas();
        setContentPane(canvas);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setSize(850, 850);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public AlgoFrame(String title){

        this(title, 1024, 768, null);
    }

    public int getCanvasWidth(){return canvasWidth;}
    public int getCanvasHeight(){return canvasHeight;}

    // 存储坐标（窗口中的位置）
    private ArrayList<Point> points;
    public void render(ArrayList<Point> points){
        this.points = points;
        repaint();
    }

    private class AlgoCanvas extends JPanel{

        public AlgoCanvas(){
            // 双缓存
            super(true);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D)g;
            g2d.setColor(Color.black);
            g2d.fillRect(0, 0, canvasWidth, canvasHeight);
            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);

            // 具体绘制
            // TODO： 绘制自己的数据data
            // 在画布中先绘制一个圆
            AlgoVisHelper.setColor(g2d, AlgoVisHelper.Blue);
            AlgoVisHelper.setStrokeWidth(g2d, 2);
            
            g2d.draw(circle);
            AlgoVisHelper.setStrokeWidth(g2d, 1);
            for(int i = 0; i < points.size(); i++) {
            	AlgoVisHelper.setColor(g2d, AlgoVisHelper.Blue);
            	if(circle.contains(points.get(i))) {
            		AlgoVisHelper.setColor(g2d, AlgoVisHelper.Yellow);
            	} else {
            		AlgoVisHelper.setColor(g2d, AlgoVisHelper.Red);
            	}
            	AlgoVisHelper.fillCircle(g2d, points.get(i).x, points.get(i).y, 3);
            }
            
        }

        @Override
        public Dimension getPreferredSize(){
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


