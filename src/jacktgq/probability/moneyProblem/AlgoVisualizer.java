package jacktgq.probability.moneyProblem;

import jacktgq.AlgoVisHelper;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.util.Arrays;

public class AlgoVisualizer {

    // TODO: 创建自己的数据
    private int[] money;        // 数据
    private AlgoFrame frame;    // 视图

    public AlgoVisualizer(int sceneWidth, int sceneHeight){

    	// 初始化用户的所拥有的金额，假设每人分配100元
    	money = new int[100];
    	for(int i = 0; i < 100; i++) {
    		money[i] = 100;
    	}
    	
    	//所有用户随机给其他用户一块钱
        moneyAllocate();
    	
        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("Welcome", sceneWidth, sceneHeight, money);
            // TODO: 根据情况决定是否加入键盘鼠标事件监听器
            frame.addKeyListener(new AlgoKeyListener());
            frame.addMouseListener(new AlgoMouseListener());
            new Thread(() -> {
                run();
            }).start();
        });
    }
    //所有用户随机给其他用户一块钱
	private void moneyAllocate() {
    	//生成一个和money等长的随机下标的数组，用于模拟为每一个用户选择另外一个用户然后给他一块钱
    	//int[] randomArrayIndexes = RandomIndexUtils.getRandomArrayIndexes(money.length);
		for(int k = 0; k < 50; k++) {
			for(int i = 0; i < money.length; i++) {
				//if(money[i] > 0) {
					int randomIndex;
					do {
						randomIndex = (int) (Math.random() * 100);
					} while(randomIndex == i);
					money[i]--;//当前用户金额减一
					//money[randomArrayIndexes[i]]++; //金额随机给了另外一个用户，这个用户的金额加一
					money[randomIndex]++;
				//}
			}
		}
	}

    // 动画逻辑
    private void run(){
    	while(true) {
    		moneyAllocate();
    		Arrays.sort(money);
    		frame.render(money);
    		AlgoVisHelper.pause(30);
    	}
    }

    // TODO: 根据情况决定是否实现键盘鼠标等交互事件监听器类
    private class AlgoKeyListener extends KeyAdapter{ }
    private class AlgoMouseListener extends MouseAdapter{ }

    public static void main(String[] args) {

        int sceneWidth = 1000;
        int sceneHeight = 800;

        // TODO: 根据需要设置其他参数，初始化visualizer
        AlgoVisualizer visualizer = new AlgoVisualizer(sceneWidth, sceneHeight);
    }
}
