package jacktgq.probability.moneyProblem;
import jacktgq.AlgoVisHelper;

import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.RenderingHints;

import javax.swing.*;

public class AlgoFrame extends JFrame{

    private int canvasWidth;
    private int canvasHeight;

    public AlgoFrame(String title, int canvasWidth, int canvasHeight, int[] money){

        super(title);

        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.money = money;

        AlgoCanvas canvas = new AlgoCanvas();
        setContentPane(canvas);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public AlgoFrame(String title){

        this(title, 1024, 768, null);
    }

    public int getCanvasWidth(){return canvasWidth;}
    public int getCanvasHeight(){return canvasHeight;}

    // TODO: 设置自己的数据
    private int[] money;
    public void render(int[] money){
        this.money = money;
        repaint();
    }

    private class AlgoCanvas extends JPanel{

        public AlgoCanvas(){
            // 双缓存
            super(true);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D)g;

            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);

            // 具体绘制
            // TODO： 绘制自己的数据data
            int w = canvasWidth / money.length;
            int moneyPerHeight = (int) (canvasHeight / 600.0);
            for(int i = 0; i < money.length; i++) {
            	if(money[i] > 0) {
            		AlgoVisHelper.setColor(g2d, AlgoVisHelper.Blue);
            		AlgoVisHelper.fillRectangle(g2d, w * i + 5, canvasHeight / 2 - money[i] * moneyPerHeight, w - 1, money[i] * moneyPerHeight);
            	} else {
            		AlgoVisHelper.setColor(g2d, AlgoVisHelper.Red);
            		AlgoVisHelper.fillRectangle(g2d, w * i + 5, canvasHeight / 2, w - 1, -money[i] * moneyPerHeight);
            	}
            }
        }

        @Override
        public Dimension getPreferredSize(){
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


