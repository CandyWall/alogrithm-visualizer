package jacktgq.mazeSolver.generator;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @Author CandyWall
 * @Date 2021/3/27--16:56
 * @Description 优化后更具随机性的随机队列
 */
public class EnhancedRandomQueue<E> {
    private LinkedList<E> queue;
    public EnhancedRandomQueue() {
        queue = new LinkedList<>();
    }

    /**
     * 元素入队
     * @param e
     */
    public void enQueue(E e) {
        if (Math.random() < 0.5) {
            queue.addFirst(e);
        } else {
            queue.addLast(e);
        }
    }

    /**
     * 元素出队
     */
    public E deQueue() {
        if (isEmpty()) {
            throw new IllegalArgumentException("当前队列为空，不能出队！");
        }
        if (Math.random() < 0.5) {
            return queue.removeFirst();
        }

        return queue.removeLast();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
