package jacktgq.mazeSolver.generator;

import java.util.ArrayList;

/**
 * @Author CandyWall
 * @Date 2021/3/27--16:56
 * @Description 随机队列
 */
public class RandomQueue<E> {
    private ArrayList<E> queue;
    public RandomQueue() {
        queue = new ArrayList<>();
    }

    /**
     * 元素入队
     * @param e
     */
    public void enQueue(E e) {
        queue.add(e);
    }

    /**
     * 元素出队
     */
    public E deQueue() {
        if (isEmpty()) {
            throw new IllegalArgumentException("当前队列为空，不能出队！");
        }
        // 随机选出一个元素和最后一个元素交换位置
        int randomIndex = (int) (Math.random() * queue.size());
        swap(randomIndex, queue.size() - 1);

        // 最后一个元素出队
        return queue.remove(queue.size() - 1);
    }

    /**
     * 交换两个元素的位置
     * @param i
     * @param j
     */
    public void swap(int i, int j) {
        E temp = queue.get(i);
        queue.set(i, queue.get(j));
        queue.set(j, temp);
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
