package jacktgq.mazeSolver.generator;

/**
 * @Author CandyWall
 * @Date 2021/3/26--16:06
 * @Description 记录某个格子的位置以及上一个元素的位置
 */
public class Position {
    public int x;
    public int y;
    public Position prev;

    public Position(int x, int y, Position prev) {
        this.x = x;
        this.y = y;
        this.prev = prev;
    }

    public Position(int x, int y) {
        this(x, y, null);
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                ", prev=" + prev +
                '}';
    }
}
