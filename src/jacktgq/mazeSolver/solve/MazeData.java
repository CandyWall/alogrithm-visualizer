package jacktgq.mazeSolver.solve;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @Author CandyWall
 * @Date 2021/3/26--0:30
 * @Description 迷宫数据类
 */
public class MazeData {
    public static final char ROAD = ' ';
    public static final char WALL = '#';
    private int entranceX, entranceY;
    private int exitX, exitY;
    private int M, N;
    private char[][] maze;
    public boolean[][] visited;
    public boolean[][] path;
    public boolean[][] result;

    public MazeData(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("文件名不能为空！");
        }
        File file = new File(filename);
        if (!file.exists()) {
            throw new IllegalArgumentException("文件不存在！");
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            // 首先读取第一行的行数和列数
            String line = reader.readLine();
            String[] strs = line.split(" ");
            M = Integer.parseInt(strs[0]);
            N = Integer.parseInt(strs[1]);
            maze = new char[M][N];

            // 读取M行数据
            for (int i = 0; i < M; i++) {
                line = reader.readLine();
                // 每行保证有N个字符
                if (line.length() != N) {
                    throw new IllegalArgumentException("第" + i + " 行元素个数不足 " + N + "个！");
                }
                // 每一行的又可以拆分成N个字符
                for (int j = 0; j < N; j++) {
                    maze[i][j] = line.charAt(j);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        visited = new boolean[M][N];
        path = new boolean[M][N];
        result = new boolean[M][N];
        entranceX = 1;
        entranceY = 0;
        exitX = M - 2;
        exitY = N - 1;
    }

    /*public static void main(String[] args) {
        System.out.println(new MazeData("data/maze_101_101.txt"));
    }*/

    public int getM() {
        return M;
    }

    public int getN() {
        return N;
    }

    public char[][] getMaze() {
        return maze;
    }

    /**
     * 获取迷宫中某个位置的元素
     * @param x
     * @param y
     * @return
     */
    public char getMazeDataAt(int x, int y) {
        return maze[x][y];
    }

    public boolean inArea(int x, int y) {
        return x >= 0 && x < M && y >=0 && y < N;
    }

    public int getEntranceX() {
        return entranceX;
    }

    public int getEntranceY() {
        return entranceY;
    }

    public int getExitX() {
        return exitX;
    }

    public int getExitY() {
        return exitY;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(M + " " + N + "\n");
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                sb.append(maze[i][j]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
