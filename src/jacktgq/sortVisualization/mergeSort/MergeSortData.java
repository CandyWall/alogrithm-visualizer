package jacktgq.sortVisualization.mergeSort;
/**
 * 
 * @Title: SelectionSortData.java 
 * @Package top.jacktgq.sortVisualization.sort 
 * @Description: 归并排序需要用到的数据
 * @author CandyWall   
 * @date 2020年10月15日 下午12:31:03 
 * @version V1.0
 */
public class MergeSortData {
	private int[] numbers;

	public int l, r;		//每次归并排序的左右边界的索引值
	public int mergeIndex;	//归并的时候两个子数组归并成一个数组时候的下标

	public MergeSortData(int n, int randomBound) {
		numbers = new int[n];
		for(int i = 0; i < n; i++)
			numbers[i] = (int) (Math.random() * randomBound) + 1;
	}
	
	//获取待数据的个数
	public int size() {
		return numbers.length;
	}
	
	public int get(int index) {
		if(index < 0 || index >= numbers.length)
			throw new IllegalArgumentException("访问的数据下标越界，index：" + index);
		return numbers[index];
	}
	
	public void set(int index, int value) {
		if(index < 0 || index > numbers.length) {
			throw new IllegalArgumentException("插入的数据下标越界，index：" + index);
		}
		numbers[index] = value;
	}

	public void swap(int i, int j) {
		int temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}
}
