package jacktgq.sortVisualization.mergeSort;

import jacktgq.AlgoVisHelper;

import java.awt.*;

public class AlgoVisualizer {

    // TODO: 创建自己的数据
    private MergeSortData data;        // 数据
    private AlgoFrame frame;    // 视图

    public AlgoVisualizer(int sceneWidth, int sceneHeight, int n){
    	// 初始化待排序数据
    	data = new MergeSortData(n, 500);
    	
        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("插入排序算法可视化", sceneWidth, sceneHeight, data);
            new Thread(() -> {
                run();
            }).start();
        });
    }

    // 动画逻辑
    private void run(){
    	setData(-1, -1, -1);
    	mergeSort(0, data.size() - 1);
    	setData(0, data.size() - 1, data.size() - 1);
    }
    
    private void mergeSort(int l, int r) {
    	if(l >= r)
    		return;
    	setData(l, r, -1);
    	int mid = (l + r) / 2;
    	mergeSort(l, mid);
    	mergeSort(mid + 1, r);
    	merge(l, mid, r);
	}

    //具体的归并过程
	private void merge(int l, int mid, int r) {
		int i = l, j = mid + 1, k;
		int[] temp = new int[r - l + 1];
		for(k = 0; k < temp.length; k++) {
			temp[k] = data.get(l + k);
		}
		for(k = l; k <= r; k++) {
			if(i > mid) {					//如果左半部分元素已经全部处理完毕，就处理右半部分的
				data.set(k, temp[j - l]); j++;
			} else if(j > r) {				//如果右半部分元素已经全部处理完毕，就处理左半部分的
				data.set(k, temp[i - l]); i++;
			} else if(temp[i - l] < temp[j - l]) {	//左半部分所指元素 < 右半所指元素
				data.set(k, temp[i - l]); i++;
			} else {
				data.set(k, temp[j - l]); j++;
			}
			
			setData(l, r, k);
		}
		
	}

	private void setData(int l, int r, int mergeIndex) {
    	data.l = l;
    	data.r = r;
    	data.mergeIndex = mergeIndex;
    	
    	frame.render(data);
		AlgoVisHelper.pause(40);
    }
    
    public static void main(String[] args) {

        int sceneWidth = 806;
        int sceneHeight = 600;
        int n = 100;

        AlgoVisualizer visualizer = new AlgoVisualizer(sceneWidth, sceneHeight, n);
    }
}
