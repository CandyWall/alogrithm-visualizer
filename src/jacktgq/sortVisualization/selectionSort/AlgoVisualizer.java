package jacktgq.sortVisualization.selectionSort;
import jacktgq.AlgoVisHelper;

import java.awt.EventQueue;

public class AlgoVisualizer {

    // TODO: 创建自己的数据
    private SelectionSortData data;        // 数据
    private AlgoFrame frame;    // 视图

    public AlgoVisualizer(int sceneWidth, int sceneHeight, int n){

    	// 初始化待排序数据
    	data = new SelectionSortData(n, 500);
    	
    	
        // 初始化视图
        EventQueue.invokeLater(() -> {
            frame = new AlgoFrame("选择排序算法可视化", sceneWidth, sceneHeight, data);
            new Thread(() -> {
                run();
            }).start();
        });
    }

    // 动画逻辑
    private void run(){
    	setData(-1, -1, -1);
		//统计在圈内的点数
		for(int i = 0; i < data.size(); i++) {
			int minIndex = i;
			setData(i - 1, -1, minIndex);
			for(int  j = i + 1; j < data.size(); j++) {
				setData(i - 1, j, minIndex);
				if(data.get(minIndex) > data.get(j)) {
					minIndex = j;
					setData(i - 1, j, minIndex);
				}
				
			}
			data.swap(i, minIndex);
			setData(i, -1, -1);
			data.setOrderIndex(i);
			
		}
		
		setData(data.size() - 1, -1, -1);
    }
    
    private void setData(int orderedIndex, int currentCompareIndex, int currentMinIndex) {
    	data.setOrderIndex(orderedIndex);
    	data.setCurrentCompareIndex(currentCompareIndex);
    	data.setCurrentMinIndex(currentMinIndex);
    	
    	frame.render(data);
		AlgoVisHelper.pause(3);
    }
    
    public static void main(String[] args) {

        int sceneWidth = 806;
        int sceneHeight = 600;
        int n = 100;

        // TODO: 根据需要设置其他参数，初始化visualizer
        AlgoVisualizer visualizer = new AlgoVisualizer(sceneWidth, sceneHeight, n);
    }
}
