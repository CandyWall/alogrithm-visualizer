package jacktgq.mineSweeper;

import jacktgq.AlgoVisHelper;

import javax.swing.*;
import java.awt.*;

public class AlgoFrame extends JFrame{
    private int canvasWidth;
    private int canvasHeight;
    private MineSweeperData mineSweeperData;
    public AlgoCanvas canvas;

    public AlgoFrame(String title, int width, int height, MineSweeperData mineSweeperData){
        super(title);
        this.canvasWidth = width;
        this.canvasHeight = height;
        this.mineSweeperData = mineSweeperData;

        canvas = new AlgoCanvas();
        setContentPane(canvas);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //setSize(width + 16, height + 39);
        setSize(width + 6, height + 29);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void render(MineSweeperData mineSweeperData) {
        this.mineSweeperData = mineSweeperData;
        canvas.repaint();
    }

    public class AlgoCanvas extends JPanel{

        public AlgoCanvas(){
            // 双缓存
            super(true);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D)g;
            // 抗锯齿
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.addRenderingHints(hints);

            int m = mineSweeperData.getM();
            int n = mineSweeperData.getN();
            int block_width = canvasWidth / n;
            int block_height = canvasHeight / m;
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    Image image = null;
                    if (mineSweeperData.isOpen[i][j]) {
                        if (mineSweeperData.isMine[i][j]) {
                            image = MineSweeperData.MINE_IMAGE;
                        } else {
                            image = MineSweeperData.NUMIMAGES[mineSweeperData.getMineNumberAroundAt(i, j)];
                        }
                    } else {
                        if (mineSweeperData.isFlag[i][j]) {
                            image = MineSweeperData.FLAG_IMAGE;
                        } else {
                            image = MineSweeperData.BLOCK_IMAGE;
                        }
                    }

                    AlgoVisHelper.putImage(g2d, j * block_width, i * block_height, image);
                }
            }

            // 如果游戏结束了就绘制游戏结束的图片
            if (mineSweeperData.isGameOver()) {
                int imgWidth = MineSweeperData.GAMEOVER_IMAGE.getWidth();
                int imgHeight = MineSweeperData.GAMEOVER_IMAGE.getHeight();
                g.drawImage(MineSweeperData.GAMEOVER_IMAGE, (canvasWidth - imgWidth) / 2, (canvasHeight - imgHeight) / 2, null);
            }

            // 如果游戏获胜了就绘制你获胜了的图片
            if (mineSweeperData.isWin()) {
                int imgWidth = MineSweeperData.YOUWIN_IMAGE.getWidth();
                int imgHeight = MineSweeperData.YOUWIN_IMAGE.getHeight();
                g.drawImage(MineSweeperData.YOUWIN_IMAGE, (canvasWidth - imgWidth) / 2, (canvasHeight - imgHeight) / 2, null);
            }
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(canvasWidth, canvasHeight);
        }
    }
}


